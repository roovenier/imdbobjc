//
//  DataManager.h
//  IMDBTestObjc
//
//  Created by Александр Ощепков on 01.07.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DataManager : NSObject

@property (readonly, strong) NSPersistentContainer *persistentContainer;

+ (DataManager *)instance;
- (void)saveContext;

@end
