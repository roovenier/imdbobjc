//
//  MovieDetailController.h
//  IMDBTestObjc
//
//  Created by Александр Ощепков on 01.07.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieDetailController : UIViewController

@property (strong, nonatomic) NSString *movieTitle;
@property (strong, nonatomic) NSString *imdbID;

@end
