//
//  MovieItemModel.h
//  IMDBTestObjc
//
//  Created by Александр Ощепков on 30.06.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MovieItemModel : NSObject

@property (strong, nonatomic) NSString *imdbID;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *year;
@property (strong, nonatomic) NSURL *posterURL;

- (instancetype)initWithResponse:(NSDictionary *)responseObject;

@end
