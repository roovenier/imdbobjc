//
//  MovieDetailModel.m
//  IMDBTestObjc
//
//  Created by Александр Ощепков on 01.07.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

#import "MovieDetailModel.h"

@implementation MovieDetailModel

- (instancetype)initWithResponse:(NSDictionary *)responseObject {
    self = [super init];
    if (self) {
        self.imdbID = [responseObject objectForKey:@"imdbID"];
        self.movieTitle = [responseObject objectForKey:@"Title"];
        self.duration = [responseObject objectForKey:@"Runtime"];
        self.genres = [responseObject objectForKey:@"Genre"];
        self.releasedDate = [[self dateFromString:[responseObject objectForKey:@"Released"]] timeIntervalSince1970];
        self.country = [responseObject objectForKey:@"Country"];
        self.releasedYear = [responseObject objectForKey:@"Year"];
        self.rating = [[responseObject objectForKey:@"Ratings"] count] > 0 ? [[[responseObject objectForKey:@"Ratings"] objectAtIndex:0] objectForKey:@"Value"] : @"N/A";
        self.director = [responseObject objectForKey:@"Director"];
        self.writers = [responseObject objectForKey:@"Writer"];
        self.actors = [responseObject objectForKey:@"Actors"];
        self.type = [responseObject objectForKey:@"Type"];
        self.movieDescription = [responseObject objectForKey:@"Plot"];
        self.posterURL = [NSURL URLWithString:[responseObject objectForKey:@"Poster"]];
    }
    return self;
}

- (NSDate *)dateFromString:(NSString *)dateStr {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:@"d MMM yyyy"];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormat setLocale:locale];
    
    return [dateFormat dateFromString:dateStr];
}

@end
