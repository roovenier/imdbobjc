//
//  main.m
//  IMDBTestObjc
//
//  Created by Александр Ощепков on 29.06.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
