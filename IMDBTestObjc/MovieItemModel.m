//
//  MovieItemModel.m
//  IMDBTestObjc
//
//  Created by Александр Ощепков on 30.06.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

#import "MovieItemModel.h"

@implementation MovieItemModel

- (instancetype)initWithResponse:(NSDictionary *)responseObject {
    self = [super init];
    if (self) {
        self.imdbID = [responseObject objectForKey:@"imdbID"];
        self.title = [responseObject objectForKey:@"Title"];
        self.type = [responseObject objectForKey:@"Type"];
        self.year = [responseObject objectForKey:@"Year"];
        self.posterURL = [NSURL URLWithString:[responseObject objectForKey:@"Poster"]];
    }
    return self;
}

@end
