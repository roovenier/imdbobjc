//
//  MovieCell.m
//  IMDBTestObjc
//
//  Created by Александр Ощепков on 29.06.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

#import "MovieCell.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@implementation MovieCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.wrapperView.layer.borderColor = [UIColorFromRGB(0xDDDDDD) CGColor];
    self.wrapperView.layer.borderWidth = 1.f;
    
    self.wrapperView.translatesAutoresizingMaskIntoConstraints = NO;
}

@end
