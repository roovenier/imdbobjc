//
//  BookmarksController.m
//  IMDBTestObjc
//
//  Created by Александр Ощепков on 01.07.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

#import "BookmarksController.h"
#import "MovieDetailController.h"
#import "DataManager.h"
#import "Bookmarks+CoreDataProperties.h"
#import "UIImageView+AFNetworking.h"
#import "Bookmarks+CoreDataProperties.h"

#import "MovieCell.h"
#import "MovieItemModel.h"

@interface BookmarksController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *moviesTableView;
@property (strong, nonatomic) NSMutableArray *moviesArray;
@property (weak, nonatomic) IBOutlet UILabel *noBookmarksLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end

@implementation BookmarksController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.managedObjectContext = [[[DataManager instance] persistentContainer] viewContext];
    
    self.moviesArray = [NSMutableArray array];
    
    self.moviesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.moviesTableView.estimatedRowHeight = 44;
    self.moviesTableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self moviesFromStore];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)editingRowsAction:(UIBarButtonItem *)sender {
    if(!self.moviesTableView.editing) {
        [self.moviesTableView setEditing:YES animated:YES];
        sender.title = @"Done";
        sender.style = UIBarButtonItemStyleDone;
    } else {
        [self.moviesTableView setEditing:NO animated:YES];
        sender.title = @"Edit";
        sender.style = UIBarButtonItemStylePlain;
    }
}

- (void)editButtonControl {
    if(self.moviesArray.count == 0) {
        [self.editButton setEnabled:NO];
        [self.editButton setTintColor:[UIColor clearColor]];
    } else {
        [self.editButton setEnabled:YES];
        [self.editButton setTintColor:nil];
    }
}

- (void)moviesFromStore {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Bookmarks"];
    [request setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]]];
    
    NSError *error = nil;
    NSArray *results = [self.managedObjectContext executeFetchRequest:request error:&error];
    
    if(error) {
        NSLog(@"Error fetching Bookmarks objects: %@\n%@", [error localizedDescription], [error userInfo]);
    } else {
        self.moviesArray = [NSMutableArray arrayWithArray:results];
        
        if ([results count] > 0) {
            self.noBookmarksLabel.hidden = YES;
        } else {
            self.noBookmarksLabel.hidden = NO;
        }
        
        [self.moviesTableView reloadData];
        
        [self editButtonControl];
    }
}

- (void)removeMovieFromStore:(NSIndexPath *)indexPath {
    [self.managedObjectContext deleteObject:[self.moviesArray objectAtIndex:indexPath.row]];
    
    NSError * error = nil;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error: %@", [error localizedDescription]);
    } else {
        [self.moviesTableView beginUpdates];
        [self.moviesTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.moviesArray removeObjectAtIndex:indexPath.row];
        [self.moviesTableView endUpdates];
        
        [self editButtonControl];
        
        if([self.moviesArray count] == 0) {
            self.noBookmarksLabel.hidden = NO;
            [self.moviesTableView setEditing:NO animated:YES];
            self.navigationItem.leftBarButtonItem.title = @"Edit";
            self.navigationItem.leftBarButtonItem.style = UIBarButtonItemStylePlain;
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.moviesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MovieCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"MovieCell" owner:self options:nil] objectAtIndex:0];
    
    Bookmarks *bookmarkedMovie = [self.moviesArray objectAtIndex:indexPath.row];
    
    cell.titleLabel.text = bookmarkedMovie.title;
    cell.yearLabel.text = bookmarkedMovie.year;
    
    if(bookmarkedMovie.poster) {
        UIImage *posterImage = [UIImage imageWithData:bookmarkedMovie.poster];
        cell.posterImage.image = posterImage;
    } else {
        [cell.posterImage removeFromSuperview];
        NSLayoutConstraint *yearBottomConstraint = [NSLayoutConstraint constraintWithItem:cell.wrapperView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:cell.yearLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:18];
        [cell.wrapperView addConstraint:yearBottomConstraint];
    }
    
    UIImage *typeImage = [UIImage imageNamed:bookmarkedMovie.type];
    cell.typeImage.image = typeImage;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    MovieItemModel *movie = [self.moviesArray objectAtIndex:indexPath.row];
    
    MovieDetailController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MovieDetailController"];
    vc.imdbID = movie.imdbID;
    vc.movieTitle = movie.title;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(editingStyle == UITableViewCellEditingStyleDelete) {
        [self removeMovieFromStore:indexPath];
    }
}

@end
