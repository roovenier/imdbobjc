//
//  MovieDetailModel.h
//  IMDBTestObjc
//
//  Created by Александр Ощепков on 01.07.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MovieDetailModel : NSObject

@property (strong, nonatomic) NSString *imdbID;
@property (strong, nonatomic) NSString *movieTitle;
@property (strong, nonatomic) NSString *duration;
@property (strong, nonatomic) NSString *genres;
@property (assign, nonatomic) NSTimeInterval releasedDate;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *releasedYear;
@property (strong, nonatomic) NSString *rating;
@property (strong, nonatomic) NSString *director;
@property (strong, nonatomic) NSString *writers;
@property (strong, nonatomic) NSString *actors;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *movieDescription;
@property (strong, nonatomic) NSURL *posterURL;
@property (assign, nonatomic) BOOL isBookmarked;

- (instancetype)initWithResponse:(NSDictionary *)responseObject;

@end
