//
//  MovieDetailController.m
//  IMDBTestObjc
//
//  Created by Александр Ощепков on 01.07.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

#import "MovieDetailController.h"
#import "UIImageView+AFNetworking.h"
#import "ServerManager.h"
#import "Bookmarks+CoreDataProperties.h"
#import "DataManager.h"
#import "SharingController.h"

#import "MovieDetailModel.h"

@interface MovieDetailController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *wrapperView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *typeImage;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *genresLabel;
@property (weak, nonatomic) IBOutlet UIView *mainInfoView;
@property (weak, nonatomic) IBOutlet UILabel *directorLabel;
@property (weak, nonatomic) IBOutlet UILabel *writersLabel;
@property (weak, nonatomic) IBOutlet UILabel *actorsLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UIView *descriptionView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionTitle;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *posterImage;
@property (weak, nonatomic) IBOutlet UIImageView *bookmarkerView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObject *bookmarkObject;
@property (strong, nonatomic) MovieDetailModel *movie;
@property (strong, nonatomic) NSString *imdbMovieURL;

@end

@implementation MovieDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.managedObjectContext = [[[DataManager instance] persistentContainer] viewContext];
    
    UIBarButtonItem *actionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionSheetAction:)];
    self.navigationItem.rightBarButtonItem = actionButton;
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    self.navigationItem.title = self.movieTitle;
    
    [self requestMovieDetail];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if(self.movie) {
        [self bookmarkBehavior];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void)keyboardWillToggle:(NSNotification *)aNotification {
    CGRect frame = [[[self tabBarController] tabBar] frame];
    CGRect keyboard = [[aNotification.userInfo valueForKey:@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
    frame.origin.y = keyboard.origin.y - frame.size.height;
    [UIView animateWithDuration:[[aNotification.userInfo valueForKey:@"UIKeyboardAnimationDurationUserInfoKey"] floatValue] animations:^{
        [[[self tabBarController] tabBar] setFrame:frame];
    }];
}

- (void)bookmarkBehavior {
    self.movie.isBookmarked = [self isMovieBookmarked];
    self.bookmarkerView.hidden = self.movie.isBookmarked == YES ? NO : YES;
}

- (BOOL)isMovieBookmarked {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Bookmarks" inManagedObjectContext:self.managedObjectContext]];
    [request setPredicate:[NSPredicate predicateWithFormat:@"imdbID = %@", self.imdbID]];
    [request setIncludesSubentities:NO];
    
    NSError *error = nil;
    NSArray *results = [self.managedObjectContext executeFetchRequest:request error:&error];
    
    if(error) {
        NSLog(@"Error: %@", error);
        return NO;
    }
    
    if([results count] == 0) {
        return NO;
    } else {
        self.bookmarkObject = [results firstObject];
        return YES;
    }
}

- (void)actionSheetAction:(UIBarButtonItem *)sender {
    __weak MovieDetailController *weakSelf = self;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:self.movieTitle message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Facebook share" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if(self.imdbMovieURL) {
            SharingController *vc = [weakSelf.storyboard instantiateViewControllerWithIdentifier:@"SharingController"];
            
            NSURL *urlRequest = [NSURL URLWithString:[@"https://www.facebook.com/sharer/sharer.php?u=" stringByAppendingString:weakSelf.imdbMovieURL]];
            
            vc.navigationTitle = @"Facebook share";
            vc.urlForRequest = urlRequest;
            
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Twitter share" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if(self.imdbMovieURL) {
            SharingController *vc = [weakSelf.storyboard instantiateViewControllerWithIdentifier:@"SharingController"];
            
            NSURL *urlRequest = [NSURL URLWithString:[@"http://twitter.com/share?url=" stringByAppendingString:weakSelf.imdbMovieURL]];
            
            vc.navigationTitle = @"Twitter share";
            vc.urlForRequest = urlRequest;
            
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }
    }]];
    
    if(!self.movie.isBookmarked) {
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Add to Bookmarks" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            Bookmarks *bookmarkedMovie = [NSEntityDescription insertNewObjectForEntityForName:@"Bookmarks" inManagedObjectContext:weakSelf.managedObjectContext];
            
            NSData *posterData = UIImagePNGRepresentation(weakSelf.posterImage.image);
            
            bookmarkedMovie.imdbID = weakSelf.movie.imdbID;
            bookmarkedMovie.title = weakSelf.movie.movieTitle;
            bookmarkedMovie.type = weakSelf.movie.type;
            bookmarkedMovie.year = weakSelf.movie.releasedYear;
            bookmarkedMovie.poster = posterData;
            bookmarkedMovie.creationDate = [NSDate date];
            
            NSError *error = nil;
            if ([weakSelf.managedObjectContext save:&error] == NO) {
                NSLog(@"Error saving context: %@\n%@", [error localizedDescription], [error userInfo]);
            } else {
                weakSelf.bookmarkObject = bookmarkedMovie;
                weakSelf.movie.isBookmarked = YES;
                weakSelf.bookmarkerView.hidden = NO;
            }
        }]];
    } else {
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Remove from Bookmarks" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            [self.managedObjectContext deleteObject:self.bookmarkObject];
            
            NSError * error = nil;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"Error: %@", [error localizedDescription]);
            } else {
                self.movie.isBookmarked = NO;
                self.bookmarkerView.hidden = YES;
            }
        }]];
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)requestMovieDetail {
    [[ServerManager instance] getMovieDetail:self.imdbID onSuccess:^(MovieDetailModel *movie) {
        self.movie = movie;
        [self bookmarkBehavior];
        self.imdbMovieURL = [@"http://imdb.com/title/" stringByAppendingString:self.movie.imdbID];
        [self setOutletsWithValues:movie];
    } onFailure:^(NSError *error) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *returnButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alert addAction:returnButton];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

- (void)setOutletsWithValues:(MovieDetailModel *)movie {
    self.indicatorView.hidden = YES;
    self.scrollView.hidden = NO;
    self.posterImage.image = nil;
    
    self.titleLabel.text = movie.movieTitle;
    self.infoLabel.text = [NSString stringWithFormat:@"%@, %@ (%@)", movie.duration, [self dateStringFromDateObject:movie.releasedDate], movie.country];
    self.genresLabel.text = movie.genres;
    self.directorLabel.text = [@"Director: " stringByAppendingString:movie.director];
    self.writersLabel.text = [@"Writers: " stringByAppendingString:movie.writers];
    self.actorsLabel.text = [@"Actors: " stringByAppendingString:movie.actors];
    self.ratingLabel.text = [@"Rating: " stringByAppendingString:movie.rating];
    self.descriptionTitle.hidden = NO;
    self.descriptionLabel.text = movie.movieDescription;
    self.typeImage.image = [UIImage imageNamed:movie.type];
    
    __weak MovieDetailController *weakSelf = self;
    
    if(![[NSString stringWithFormat:@"%@", movie.posterURL] isEqualToString:@"N/A"]) {
        NSURLRequest *requestPoster = [[NSURLRequest alloc] initWithURL:movie.posterURL];
        [self.posterImage setImageWithURLRequest:requestPoster placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            weakSelf.posterImage.image = image;
        } failure:nil];
    } else {
        [self.posterImage removeFromSuperview];
        NSLayoutConstraint *descriptionBottomConstraint = [NSLayoutConstraint constraintWithItem:self.wrapperView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.descriptionView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:30];
        [self.wrapperView addConstraint:descriptionBottomConstraint];
    }
}

- (NSString *)dateStringFromDateObject:(NSTimeInterval)timestamp {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d MMM yyyy"];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    
    return [formatter stringFromDate:date];
}

@end
