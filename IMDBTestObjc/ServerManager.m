//
//  ServerManager.m
//  IMDBTestObjc
//
//  Created by Александр Ощепков on 29.06.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

#import "ServerManager.h"
#import "AFNetworking.h"

#import "MovieItemModel.h"
#import "MovieDetailModel.h"

@interface ServerManager ()

@property (strong, nonatomic) AFHTTPSessionManager *httpManager;

@end

@implementation ServerManager

static NSString *apiKey = @"28e5c847";

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.httpManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://omdbapi.com/"]];
    }
    return self;
}

+ (ServerManager *)instance {
    static ServerManager *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[ServerManager alloc] init];
    });
    
    return instance;
}

- (void)getMoviesBySearch:(NSString *)searchValue onSuccess:(void(^)(NSArray *movies))success onFailure:(void(^)(NSError *error))failure {
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:searchValue, @"s", apiKey, @"apikey", nil];
    
    [self.httpManager GET:@"" parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSMutableArray *moviesArray = [NSMutableArray array];
        
        for(NSDictionary *itemDictionary in [responseObject objectForKey:@"Search"]) {
            MovieItemModel *movie = [[MovieItemModel alloc] initWithResponse:itemDictionary];
            
            [moviesArray addObject:movie];
        }
        
        success(moviesArray);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getMovieDetail:(NSString *)imdbID onSuccess:(void(^)(MovieDetailModel *movie))success onFailure:(void(^)(NSError *error))failure {
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:imdbID, @"i", apiKey, @"apikey", nil];
    
    [self.httpManager GET:@"" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        MovieDetailModel *movie = [[MovieDetailModel alloc] initWithResponse:responseObject];
        
        success(movie);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}

@end
