//
//  ViewController.m
//  IMDBTestObjc
//
//  Created by Александр Ощепков on 29.06.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

#import "SearchController.h"
#import "UIImageView+AFNetworking.h"
#import "ServerManager.h"

#import "MovieItemModel.h"
#import "MovieCell.h"
#import "MovieDetailController.h"

@interface SearchController () <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *moviesTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBarView;
@property (weak, nonatomic) IBOutlet UILabel *nothingFoundLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeForSearchLabel;
@property (strong, nonatomic) NSArray *moviesArray;
@property (assign, nonatomic) CGRect defaultTableFrame;

@end

@implementation SearchController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.defaultTableFrame = self.moviesTableView.frame;
    
    self.moviesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.moviesTableView.estimatedRowHeight = 44;
    self.moviesTableView.rowHeight = UITableViewAutomaticDimension;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification  object:nil];
}

#pragma mark - Actions

- (void)keyboardWillShown:(NSNotification *)aNotification {
    CGRect tabsFrame = [[[self tabBarController] tabBar] frame];
    CGRect keyboard = [[aNotification.userInfo valueForKey:@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];

    CGFloat newTableHeight = self.moviesTableView.bounds.size.height - keyboard.size.height + tabsFrame.size.height;
    CGRect newTableFrame = CGRectMake(self.moviesTableView.frame.origin.x, self.moviesTableView.frame.origin.y, self.moviesTableView.frame.size.width, newTableHeight);
    
    self.moviesTableView.frame = newTableFrame;
}

- (void)keyboardWillHide:(NSNotification *)aNotification {
    self.moviesTableView.frame = self.defaultTableFrame;
}

- (void)searchMoviesRequest {
    [[ServerManager instance] getMoviesBySearch:self.searchBarView.text onSuccess:^(NSArray *movies) {
        if([movies count] > 0) {
            self.moviesArray = movies;
            
            self.typeForSearchLabel.hidden = YES;
            self.nothingFoundLabel.hidden = YES;
            self.moviesTableView.hidden = NO;
            
            [self.moviesTableView reloadData];
        } else {
            self.moviesTableView.hidden = YES;
            self.typeForSearchLabel.hidden = YES;
            self.nothingFoundLabel.hidden = NO;
        }
    } onFailure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
    }];
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if([searchText length] == 0) {
        self.moviesArray = nil;
        [self.moviesTableView reloadData];
        self.nothingFoundLabel.hidden = YES;
        self.typeForSearchLabel.hidden = NO;
    } else {
        [self searchMoviesRequest];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text = nil;
    
    [self.searchBarView resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self searchMoviesRequest];
    
    [self.searchBarView resignFirstResponder];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.moviesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MovieCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"MovieCell" owner:self options:nil] objectAtIndex:0];
    
    MovieItemModel *movie = [self.moviesArray objectAtIndex:indexPath.row];
    
    cell.titleLabel.text = movie.title;
    cell.yearLabel.text = movie.year;
    
    __weak MovieCell *weakCell = cell;
    
    if(![[NSString stringWithFormat:@"%@", movie.posterURL] isEqualToString:@"N/A"]) {
        NSURLRequest *posterRequest = [NSURLRequest requestWithURL:movie.posterURL];
        [cell.posterImage setImageWithURLRequest:posterRequest placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            weakCell.posterImage.image = image;
        } failure:nil];
    } else {
        [cell.posterImage removeFromSuperview];
        NSLayoutConstraint *yearBottomConstraint = [NSLayoutConstraint constraintWithItem:cell.wrapperView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:cell.yearLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:18];
        [cell.wrapperView addConstraint:yearBottomConstraint];
    }
    
    UIImage *typeImage = [UIImage imageNamed:movie.type];
    cell.typeImage.image = typeImage;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    MovieItemModel *movie = [self.moviesArray objectAtIndex:indexPath.row];
    
    MovieDetailController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MovieDetailController"];
    vc.imdbID = movie.imdbID;
    vc.movieTitle = movie.title;
    
    [self.navigationController pushViewController:vc animated:YES];
}

@end
