//
//  ServerManager.h
//  IMDBTestObjc
//
//  Created by Александр Ощепков on 29.06.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MovieDetailModel;

@interface ServerManager : NSObject

+ (ServerManager *)instance;

- (void)getMoviesBySearch:(NSString *)searchValue onSuccess:(void(^)(NSArray *movies))success onFailure:(void(^)(NSError *error))failure;
- (void)getMovieDetail:(NSString *)imdbID onSuccess:(void(^)(MovieDetailModel *movie))success onFailure:(void(^)(NSError *error))failure;

@end
