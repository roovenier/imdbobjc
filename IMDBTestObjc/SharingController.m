//
//  SharingController.m
//  IMDBTestObjc
//
//  Created by Александр Ощепков on 13.07.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

#import "SharingController.h"

@interface SharingController () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;

@end

@implementation SharingController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = self.navigationTitle;
    
    if(self.urlForRequest) {
        NSURLRequest *request = [NSURLRequest requestWithURL:self.urlForRequest];
        
        [self.webView loadRequest:request];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    self.webView.hidden = YES;
    self.indicatorView.hidden = NO;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    self.indicatorView.hidden = YES;
    self.webView.hidden = NO;
}

@end
