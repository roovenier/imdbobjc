//
//  MovieCell.h
//  IMDBTestObjc
//
//  Created by Александр Ощепков on 29.06.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UIImageView *typeImage;
@property (weak, nonatomic) IBOutlet UIImageView *posterImage;
@property (weak, nonatomic) IBOutlet UIView *wrapperView;

@end
