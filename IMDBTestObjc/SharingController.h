//
//  SharingController.h
//  IMDBTestObjc
//
//  Created by Александр Ощепков on 13.07.17.
//  Copyright © 2017 Alexander Oshchepkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SharingController : UIViewController

@property (strong, nonatomic) NSString *navigationTitle;
@property (strong, nonatomic) NSURL *urlForRequest;

@end
